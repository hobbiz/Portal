<?php

namespace AppBundle\Entity\Activity;


///**
// * Class AbstractActivity
// *
// * @ORM\Table("activity")
// * @ORM\Entity(repositoryClass="")
// * @ORM\InheritanceType("JOINED")
// * @ORM\DiscriminatorColumn(name="discr", type="string")
// * @ORM\DiscriminatorMap({
// * "drinks" = "AppBundle\Entity\Activity\Shopping",
// * })
// */
abstract class AbstractActivity
{
    public $name;
    public $type;
    public $address;
    public $description;
    public $tags;
    public $weather;
    public $numberOfPersons;
    public $price;
    public $schedule;
}
