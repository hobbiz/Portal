#!/bin/bash
set -e

install() {
    composer install
    php bin/console assets:install
}

tests() {
    php bin/phpunit -c ./
}

run() {
    supervisord
}

case "$1" in
"install")
    echo "Install"
    install
    ;;
"tests")
    echo "Tests"
    tests
    ;;
"run")
    echo "Run"
    run
    ;;
*)
    echo "Custom command : $@"
    exec "$@"
    ;;
esac
