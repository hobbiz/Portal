<?php

/* AppBundle::base.html.twig */
class __TwigTemplate_5a27d7d832f0b1f2c8820b90437a917578a2141491de800d19338c376ca2291d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_553bdc2a1f8f2025f6b479c1f2a8c2cfab60545b72915a22336e56ac0edee52a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_553bdc2a1f8f2025f6b479c1f2a8c2cfab60545b72915a22336e56ac0edee52a->enter($__internal_553bdc2a1f8f2025f6b479c1f2a8c2cfab60545b72915a22336e56ac0edee52a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle::base.html.twig"));

        $__internal_b8d9a933bcf9d639baebc50d7919debf21e9d10321d0b6aa43d546661fba1ca7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8d9a933bcf9d639baebc50d7919debf21e9d10321d0b6aa43d546661fba1ca7->enter($__internal_b8d9a933bcf9d639baebc50d7919debf21e9d10321d0b6aa43d546661fba1ca7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 10
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 13
        $this->displayBlock('body', $context, $blocks);
        // line 16
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 20
        echo "    </body>
</html>
";
        
        $__internal_553bdc2a1f8f2025f6b479c1f2a8c2cfab60545b72915a22336e56ac0edee52a->leave($__internal_553bdc2a1f8f2025f6b479c1f2a8c2cfab60545b72915a22336e56ac0edee52a_prof);

        
        $__internal_b8d9a933bcf9d639baebc50d7919debf21e9d10321d0b6aa43d546661fba1ca7->leave($__internal_b8d9a933bcf9d639baebc50d7919debf21e9d10321d0b6aa43d546661fba1ca7_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_01a6217e55d5985281d1edbace2cd12768c79a26e62a87a349865ceb5185f3f5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01a6217e55d5985281d1edbace2cd12768c79a26e62a87a349865ceb5185f3f5->enter($__internal_01a6217e55d5985281d1edbace2cd12768c79a26e62a87a349865ceb5185f3f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_fe3e4d6c33c31d3a73d61a2eb8b70f4750d492afd7170d1fb4437cbdacccbb28 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fe3e4d6c33c31d3a73d61a2eb8b70f4750d492afd7170d1fb4437cbdacccbb28->enter($__internal_fe3e4d6c33c31d3a73d61a2eb8b70f4750d492afd7170d1fb4437cbdacccbb28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_fe3e4d6c33c31d3a73d61a2eb8b70f4750d492afd7170d1fb4437cbdacccbb28->leave($__internal_fe3e4d6c33c31d3a73d61a2eb8b70f4750d492afd7170d1fb4437cbdacccbb28_prof);

        
        $__internal_01a6217e55d5985281d1edbace2cd12768c79a26e62a87a349865ceb5185f3f5->leave($__internal_01a6217e55d5985281d1edbace2cd12768c79a26e62a87a349865ceb5185f3f5_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_4622af7ffdea451e402438f35b9081bc2417db2af2369c9e2413349238f203ac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4622af7ffdea451e402438f35b9081bc2417db2af2369c9e2413349238f203ac->enter($__internal_4622af7ffdea451e402438f35b9081bc2417db2af2369c9e2413349238f203ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_239aec404e83fa38d8fd089ff9b75454260fb70f40366cd068f538d5f846e9d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_239aec404e83fa38d8fd089ff9b75454260fb70f40366cd068f538d5f846e9d6->enter($__internal_239aec404e83fa38d8fd089ff9b75454260fb70f40366cd068f538d5f846e9d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "            <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/vendor.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/main.css"), "html", null, true);
        echo "\">
        ";
        
        $__internal_239aec404e83fa38d8fd089ff9b75454260fb70f40366cd068f538d5f846e9d6->leave($__internal_239aec404e83fa38d8fd089ff9b75454260fb70f40366cd068f538d5f846e9d6_prof);

        
        $__internal_4622af7ffdea451e402438f35b9081bc2417db2af2369c9e2413349238f203ac->leave($__internal_4622af7ffdea451e402438f35b9081bc2417db2af2369c9e2413349238f203ac_prof);

    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        $__internal_d15170c72a5f57cc3f6db0e1f6e105ba730629050e0101cde5ad0036fd9644a4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d15170c72a5f57cc3f6db0e1f6e105ba730629050e0101cde5ad0036fd9644a4->enter($__internal_d15170c72a5f57cc3f6db0e1f6e105ba730629050e0101cde5ad0036fd9644a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_bc1aed2dda7eee9c0acc19ff125a5e8468fdc02f8aa66f0a8f2f34f9fc69abf0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc1aed2dda7eee9c0acc19ff125a5e8468fdc02f8aa66f0a8f2f34f9fc69abf0->enter($__internal_bc1aed2dda7eee9c0acc19ff125a5e8468fdc02f8aa66f0a8f2f34f9fc69abf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 14
        echo "            <a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("test");
        echo "\">test</a>
        ";
        
        $__internal_bc1aed2dda7eee9c0acc19ff125a5e8468fdc02f8aa66f0a8f2f34f9fc69abf0->leave($__internal_bc1aed2dda7eee9c0acc19ff125a5e8468fdc02f8aa66f0a8f2f34f9fc69abf0_prof);

        
        $__internal_d15170c72a5f57cc3f6db0e1f6e105ba730629050e0101cde5ad0036fd9644a4->leave($__internal_d15170c72a5f57cc3f6db0e1f6e105ba730629050e0101cde5ad0036fd9644a4_prof);

    }

    // line 16
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_f7e8ded5e224d8ae51a5828db67fc4fc1349b73d75cbda819267df22b5a82bca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f7e8ded5e224d8ae51a5828db67fc4fc1349b73d75cbda819267df22b5a82bca->enter($__internal_f7e8ded5e224d8ae51a5828db67fc4fc1349b73d75cbda819267df22b5a82bca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_4d382a26aa0533982f951e4ad36f2baee798b5f14e5e20780e421c809d114c58 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d382a26aa0533982f951e4ad36f2baee798b5f14e5e20780e421c809d114c58->enter($__internal_4d382a26aa0533982f951e4ad36f2baee798b5f14e5e20780e421c809d114c58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 17
        echo "            <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/vendor-footer.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_4d382a26aa0533982f951e4ad36f2baee798b5f14e5e20780e421c809d114c58->leave($__internal_4d382a26aa0533982f951e4ad36f2baee798b5f14e5e20780e421c809d114c58_prof);

        
        $__internal_f7e8ded5e224d8ae51a5828db67fc4fc1349b73d75cbda819267df22b5a82bca->leave($__internal_f7e8ded5e224d8ae51a5828db67fc4fc1349b73d75cbda819267df22b5a82bca_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 18,  140 => 17,  131 => 16,  118 => 14,  109 => 13,  97 => 8,  92 => 7,  83 => 6,  65 => 5,  53 => 20,  50 => 16,  48 => 13,  41 => 10,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}
            <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"{{ asset('css/vendor.css') }}\">
            <link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"{{ asset('css/main.css') }}\">
        {% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}
            <a href=\"{{ path('test') }}\">test</a>
        {% endblock %}
        {% block javascripts %}
            <script type=\"text/javascript\" src=\"{{ asset('js/vendor-footer.js') }}\"></script>
            <script type=\"text/javascript\" src=\"{{ asset('js/main.js') }}\"></script>
        {% endblock %}
    </body>
</html>
", "AppBundle::base.html.twig", "/Users/robingodart/www/Portal/src/src/AppBundle/Resources/views/base.html.twig");
    }
}
